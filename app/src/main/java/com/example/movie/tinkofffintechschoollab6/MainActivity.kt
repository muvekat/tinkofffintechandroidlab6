package com.example.movie.tinkofffintechschoollab6

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.HandlerThread
import android.os.Looper
import android.util.Log
import com.example.movie.tinkofffintechschoollab6.observable.MyObservable
import java.util.concurrent.Callable

class MainActivity : AppCompatActivity() {

    private val mThread = HandlerThread("executorThread").apply { start() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

         MyObservable.from {
            Thread.sleep(10000)
            "10 seconds task"
        }.apply {
            observeOn(Looper.myLooper())
            subscribeOn(mThread.looper)
            subscribe { data -> Log.d("Observable1", data) }
        }

        MyObservable.from {
            Thread.sleep(500)
            "500 milliseconds task"
        }.apply {
            subscribe { data -> Log.d("Observable2", data) }
        }
    }
}
