package com.example.movie.tinkofffintechschoollab6.observable;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Callable;

public final class MyObservable<V> implements Observable<V>{

    private static final int SEND_DATA = 10;

    private static final String LOG_TAG = "MyObservable";

    private final Callable<V> mCallable;
    private Looper mSubscriberLooper = null;
    private Looper mObserverLooper = null;

    private MyObservable(Callable<V> callable){
        mCallable = callable;
    }

    @NotNull
    public static <V> MyObservable<V> from(Callable<V> callable) {
        return new MyObservable<>(callable);
    }

    @Override
    public Observable<V> subscribeOn(Looper subscribeLooper) {
        mSubscriberLooper = subscribeLooper;
        return this;
    }

    @Override
    public Observable<V> observeOn(Looper observeLooper) {
        mObserverLooper = observeLooper;
        return this;
    }

    @Override
    public void subscribe(final ObservableCallback<V> callback) {
        if (mSubscriberLooper == null)
            mSubscriberLooper = Looper.myLooper();
        if (mObserverLooper == null)
            mObserverLooper = mSubscriberLooper;

        final Handler subscriberHandler = new Handler(mSubscriberLooper);
        final Handler observerHandler = getObserverHandler(mObserverLooper, callback);

        subscriberHandler.post(getSubscriberRunnable(mCallable, observerHandler));
    }

    @NonNull
    private Handler getObserverHandler(Looper looper, final ObservableCallback<V> callback){
        return new Handler(looper, new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {

                if (msg.what == SEND_DATA){

                    V data = null;

                    try {
                        data = (V) msg.obj;
                    } catch (ClassCastException e){
                        Log.e(LOG_TAG, "Cannot cast data from object!");
                    }

                    callback.onFinish(data);

                }
                return true;
            }

        });
    }

    @NonNull
    private Runnable getSubscriberRunnable(final Callable<V> callable, final Handler observerHandler){
        return new Runnable() {
            @Override
            public void run() {
                try{

                    Log.d(LOG_TAG, "Executing callable in: " + Looper.myLooper().toString());

                    V result = callable.call();

                    Message sendMsg = observerHandler.obtainMessage(SEND_DATA, result);
                    observerHandler.sendMessage(sendMsg);

                } catch (Exception e){
                    Log.e(LOG_TAG, "Exception happened on call", e);
                }
            }
        };
    }
}
