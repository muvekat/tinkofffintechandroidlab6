package com.example.movie.tinkofffintechschoollab6.observable;

import android.os.Looper;

import java.util.concurrent.Callable;

public interface Observable<T> {
    Observable<T> subscribeOn(Looper subscribeLooper);
    Observable<T> observeOn(Looper observeLooper);
    void subscribe(ObservableCallback<T> callback);
}
