package com.example.movie.tinkofffintechschoollab6.observable;

public interface ObservableCallback<T> {
    void onFinish(T result);
}
